package main
import (
    "fmt"
    "github.com/gorilla/mux"
    "net/http"
    "encoding/json"
    "strconv"
)

func TodoIndex(w http.ResponseWriter, r *http.Request) {
    todos := Todos{
        Todo{Id: 1, Name: "Write presentation"},
        Todo{Id: 2, Name: "Host meetup"},
    }

    w.Header().Set("Content-Type", "application/json; charset=UTF-8")
    w.WriteHeader(http.StatusOK)
    if err := json.NewEncoder(w).Encode(todos); err != nil {
        panic(err)
    }
}

func TodoShow(w http.ResponseWriter, r *http.Request) {
    todos := Todos{
        Todo{Id: 1, Name: "Write presentation"},
        Todo{Id: 2, Name: "Host meetup"},
    }

    vars := mux.Vars(r)
    todoId, _ := strconv.ParseUint(vars["todoId"], 0, 64)
    fmt.Fprintln(w, "Todo show:", todoId)

    for _, t := range todos{
        if t.Id == todoId{
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(http.StatusOK)
            if err := json.NewEncoder(w).Encode(t); err != nil {
                panic(err)
            }
        }
    }
}