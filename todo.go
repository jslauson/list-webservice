package main
import "time"

type Todo struct {
    Id uint64 `json:"id"`
    Name string `json:"name"`
    Completed bool `json:"completed"`
    Category string `json:"category"`
    Created time.Time `json:"created"`
    Due time.Time `json:"due"`
}

type Todos []Todo